
extern "C"{
#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
}
#include"posture.h"
#include"BMX055.h"
#include"MA.h"
#include"TimerDriver.h"
#include"../components/motor/motor.h"
#include"LED.h"
#include"nvs_flash_class.h"
#include"controller.h"
#include"LCD.h"
#include"PID.h"
#include"MPU9250.h"

#define dt0 0.02

posture pos;
led led0(2,LEDC_CHANNEL_0,LEDC_HIGH_SPEED_MODE,led::led_hotaru);

esp_err_t event_handler(void *ctx, system_event_t *event)
{
    return ESP_OK;
}
extern "C"{
void app_main(void);
}

TaskHandle_t h_ppp_task;
void ppp_task(void*para){
	LCD lcd;
	lcd.init(GPIO_NUM_21,GPIO_NUM_22,0x3E);
	lcd.print("ppp","qqq");
	while(1){
		BaseType_t res=xTaskNotifyWait(0,0,NULL,portMAX_DELAY);
		if(res==pdPASS){
			posture_data_t data;
			pos.get_data(&data);
			//data.print();
			//data.content.estimate_data.q_pos.print();
			vect ex(1,0,0);
			ex=data.content.estimate_data.q_pos*(quat)ex*~data.content.estimate_data.q_pos;
			//ex.print();

			//printf("\n");
		}
	}
}

void app_main(void)
{
	printf("init spi bus\n");
	//init spi bus
    esp_err_t ret;
    spi_bus_config_t busconf;
    memset(&busconf,0,sizeof(busconf));
    busconf.miso_io_num=12;
    busconf.mosi_io_num=13;
    busconf.sclk_io_num=14;
    busconf.quadhd_io_num=-1;
    busconf.quadwp_io_num=-1;
    busconf.max_transfer_sz=30;
    //Initialize the SPI bus
    ret=spi_bus_initialize(HSPI_HOST, &busconf, 1);
    ESP_ERROR_CHECK(ret);
    printf("sccess\n");

    MPU9250 mpu(HSPI_HOST,15,MPU9250::accl_range_2g,MPU9250::gyro_range_2000dps,MPU9250::mag_resolution_16bit,17,0.01);

    while (true) {

        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}


/*
 * controller.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: yuta2
 */
#include"controller.h"
#include<string.h>

gpio_num_t controller_v1_push_sw_pin[4]={GPIO_NUM_27,GPIO_NUM_5,GPIO_NUM_14,GPIO_NUM_15};
gpio_num_t controller_v1_toggle_sw_pin[4]={GPIO_NUM_16,GPIO_NUM_17,GPIO_NUM_25,GPIO_NUM_26};
gpio_num_t controller_v1_slide_sw_pin[3]={GPIO_NUM_18,GPIO_NUM_23,GPIO_NUM_19};
adc1_channel_t controller_v1_joystick_channel[4]={ADC1_GPIO35_CHANNEL,ADC1_GPIO32_CHANNEL,ADC1_GPIO34_CHANNEL,ADC1_GPIO33_CHANNEL};
gpio_num_t controller_v1_led_pin=GPIO_NUM_2;
gpio_num_t controller_v1_lcd_sda_pin=GPIO_NUM_21;
gpio_num_t controller_v1_lcd_scl_pin=GPIO_NUM_22;

controller controller_ver1(controller_v1_push_sw_pin,controller_v1_toggle_sw_pin,controller_v1_slide_sw_pin,controller_v1_joystick_channel
		,controller_v1_led_pin,controller_v1_lcd_sda_pin,controller_v1_lcd_scl_pin);


void controller::get_controller_data(controller_data_t*data){
	portENTER_CRITICAL(&mux_data);
	memcpy(data,&(this->data),sizeof(controller_data_t));
	portEXIT_CRITICAL(&mux_data);
}
void controller::set_controller_data(const controller_data_t&data){
	portENTER_CRITICAL(&mux_data);
	memcpy(&(this->data),&data,sizeof(controller_data_t));
	portEXIT_CRITICAL(&mux_data);
}
controller::controller(gpio_num_t push_sw_pin[4],gpio_num_t toggle_sw_pin[4],gpio_num_t slide_sw_pin[3],adc1_channel_t joystick_channel[4]
	,gpio_num_t led_pin,gpio_num_t lcd_sda_pin,gpio_num_t lcd_scl_pin)
:push_sw_pin{push_sw_pin[0],push_sw_pin[1],push_sw_pin[2],push_sw_pin[3]},toggle_sw_pin{toggle_sw_pin[0],toggle_sw_pin[1],toggle_sw_pin[2],toggle_sw_pin[3]}
,slide_sw_pin{slide_sw_pin[0],slide_sw_pin[1],slide_sw_pin[2]},joystick_channel{joystick_channel[0],joystick_channel[1],joystick_channel[2],joystick_channel[3]}
,led_pin(led_pin),lcd_sda_pin(lcd_sda_pin),lcd_scl_pin(lcd_scl_pin),joystick_offset{0,0,0,0},mux_data(portMUX_INITIALIZER_UNLOCKED),h_update_task(NULL),joystick_buf{MA(3),MA(3),MA(3),MA(3)}
{}
void controller::update_task(void*p){
	controller* p_ctr=(controller*)p;
	while(1){
		controller_data_t data;
		data.push_sw_state[0]=gpio_get_level(p_ctr->push_sw_pin[0]);
		data.push_sw_state[1]=gpio_get_level(p_ctr->push_sw_pin[1]);
		data.push_sw_state[2]=gpio_get_level(p_ctr->push_sw_pin[2]);
		data.push_sw_state[3]=gpio_get_level(p_ctr->push_sw_pin[3]);
		data.toggle_sw_state[0]=gpio_get_level(p_ctr->toggle_sw_pin[0]);
		data.toggle_sw_state[1]=gpio_get_level(p_ctr->toggle_sw_pin[1]);
		data.toggle_sw_state[2]=gpio_get_level(p_ctr->toggle_sw_pin[2]);
		data.toggle_sw_state[3]=gpio_get_level(p_ctr->toggle_sw_pin[3]);
		data.slide_sw_state[0]=gpio_get_level(p_ctr->slide_sw_pin[0]);
		data.slide_sw_state[1]=gpio_get_level(p_ctr->slide_sw_pin[1]);
		data.slide_sw_state[2]=gpio_get_level(p_ctr->slide_sw_pin[2]);

		uint16_t adc_raw_data[4];
		adc_raw_data[0]=adc1_get_raw(p_ctr->joystick_channel[0]);
		adc_raw_data[1]=adc1_get_raw(p_ctr->joystick_channel[1]);
		adc_raw_data[2]=adc1_get_raw(p_ctr->joystick_channel[2]);
		adc_raw_data[3]=adc1_get_raw(p_ctr->joystick_channel[3]);

		for(int i=0;i<4;++i){
			data.joystick [i]= (p_ctr->joystick_buf[i].cal((float)adc_raw_data[i]/4095.0)) - (p_ctr->joystick_offset[i]);
		}

		p_ctr->set_controller_data(data);

		vTaskDelay(5/portTICK_PERIOD_MS);
	}

}
void controller::init(){
	for(int i=0;i<4;++i)gpio_set_direction(push_sw_pin[i],GPIO_MODE_INPUT);
	for(int i=0;i<4;++i)gpio_set_direction(toggle_sw_pin[i],GPIO_MODE_INPUT);
	for(int i=0;i<3;++i)gpio_set_direction(slide_sw_pin[i],GPIO_MODE_INPUT);

	adc1_config_width(ADC_WIDTH_BIT_12);//ADC精度12bit
	for(int i=0;i<4;++i)adc1_config_channel_atten(joystick_channel[i],ADC_ATTEN_DB_11);
	for(int i=0;i<4;++i){
		for(int j=0;j<joystick_buf[i].get_size();++j){
			uint16_t adc_raw_data=adc1_get_raw(joystick_channel[i]);
			joystick_offset[i]=joystick_buf[i].cal((float)adc_raw_data/4095.0);
		}
	}
	xTaskCreatePinnedToCore(update_task,"update",2048,this,12,&h_update_task,1);
}

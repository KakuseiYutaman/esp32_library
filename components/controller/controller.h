/*
 * controller.h
 *
 *  Created on: 6 Aug 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_CONTROLLER_CONTROLLER_H_
#define COMPONENTS_CONTROLLER_CONTROLLER_H_

extern "C"{
#include"driver/gpio.h"
#include"driver/adc.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
}
#include "MA.h"

struct controller_data_t{
	bool push_sw_state[4];
	bool toggle_sw_state[4];
	bool slide_sw_state[4];
	float joystick[4];
};

class controller{
	gpio_num_t push_sw_pin[4];
	gpio_num_t toggle_sw_pin[4];
	gpio_num_t slide_sw_pin[3];
	adc1_channel_t joystick_channel[4];
	gpio_num_t led_pin;
	gpio_num_t lcd_sda_pin;
	gpio_num_t lcd_scl_pin;

	float joystick_offset[4];

	controller_data_t data;
	portMUX_TYPE mux_data;
	void set_controller_data(const controller_data_t&);

	TaskHandle_t h_update_task;
	static void update_task(void*p);

	MA joystick_buf[4];
public:
	controller(gpio_num_t push_sw_pin[4],gpio_num_t toggle_sw_pin[4],gpio_num_t slide_sw_pin[3],adc1_channel_t joystick_channel[4]
		,gpio_num_t led_pin,gpio_num_t lcd_sda_pin,gpio_num_t lcd_scl_pin);
	void get_controller_data(controller_data_t*data);
	void init();
	gpio_num_t get_lcd_sda_pin()const{return lcd_sda_pin;}
	gpio_num_t get_lcd_scl_pin()const{return lcd_scl_pin;}
	gpio_num_t get_led_pin()const{return led_pin;}
};

extern controller controller_ver1;

#endif /* COMPONENTS_CONTROLLER_CONTROLLER_H_ */

/*
 * posture.h
 *
 *  Created on: 4 Aug 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_POSTURE_POSTURE_H_
#define COMPONENTS_POSTURE_POSTURE_H_
#include"quat.h"

enum posture_data_type_t{
	posture_estimate_data,
	posture_static_calib_data,
	posture_dynamic_calib_data,
};
struct posture_estimate_data_t{
	posture_estimate_data_t(){}
	posture_estimate_data_t(const vect&accl,const vect&gyro,const vect&mag,const quat&q_pos):accl(accl),gyro(gyro),mag(mag),q_pos(q_pos){}
	vect accl,gyro,mag;
	quat q_pos;
};
struct posture_static_calib_data_t{
	posture_static_calib_data_t(){}
	posture_static_calib_data_t(const vect&accl,const vect&gyro,const vect&mag,const vect&g0,const vect&gyro_offset,const vect&mag0):accl(accl),gyro(gyro),mag(mag),g0(g0),gyro_offset(gyro_offset),mag0(mag0){}
	vect accl,gyro,mag,g0,gyro_offset,mag0;
};
struct posture_dynamic_calib_data_t{
	posture_dynamic_calib_data_t(){}
	posture_dynamic_calib_data_t(const vect&mag,const vect&mag_offset):mag(mag),mag_offset(mag_offset){}
	vect mag,mag_offset;
};
union posture_data_content_t{
	posture_data_content_t(){}
	posture_data_content_t(const posture_static_calib_data_t&static_calib_data):static_calib_data(static_calib_data){}
	posture_data_content_t(const posture_dynamic_calib_data_t&dynamic_calib_data):dynamic_calib_data(dynamic_calib_data){}
	posture_data_content_t(const posture_estimate_data_t&estimate_data):estimate_data(estimate_data){}
	posture_static_calib_data_t static_calib_data;
	posture_dynamic_calib_data_t dynamic_calib_data;
	posture_estimate_data_t estimate_data;
};
struct posture_data_t{
	posture_data_type_t type;
	posture_data_content_t content;
	void print()const;
};


class posture{
	float dt;
	vect g0,mag0;
	quat q_pos;

	enum posture_state{
		state_estimate,
		state_static_calib,
		state_dynamic_calib,
	};
	posture_state state;
	quat cal_pos(const vect&accl,const vect&gyro, const vect&mag);
	bool f_enable_accl_correct,f_enable_mag_correct;
public:
	posture();
	bool init(float dt);
	void get_data( posture_data_t* data);
	void start_estimate();
	void start_static_calib();
	void start_dynamic_calib();
	void set_g0(const vect&v);
	void set_gyro_offset(const vect&v);
	void set_mag0(const vect&v);
	void set_mag_offset(const vect&);
	void set_dt(float dt){this->dt=dt;}
	void set_q_pos(const quat&pos){q_pos=pos;}
	vect get_g0()const;
	vect get_mag0()const;
	vect get_gyro_offset()const;
	vect get_mag_offset()const;
	void enable_accl_correct();
	void disable_accl_correction();
	void enable_mag_correct();
	void disable_mag_correct();
};





#endif /* COMPONENTS_POSTURE_POSTURE_H_ */

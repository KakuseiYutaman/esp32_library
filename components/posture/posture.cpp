/*
 * posture.cpp
 *
 *  Created on: 4 Aug 2020
 *      Author: yuta2
 */

#include"posture.h"
#include"BMX055.h"
#include"nvs_flash_class.h"

#define NVS_POSTURE_NAME_SPACE "posture"

const int static_calib_buf_len=1000;
VMA static_calib_accl_buf(static_calib_buf_len),static_calib_gyro_buf(static_calib_buf_len),static_calib_mag_buf(static_calib_buf_len);

bool f_first_dynamic_calib=0;
vect mag_min,mag_max;

void posture_data_t::print()const{
	switch(type){
	case posture_estimate_data:
		printf("posture_estimate:q_pos=");
		content.estimate_data.q_pos.print();
		printf(",accl=");
		content.estimate_data.accl.print();
		printf(",gyro=");
		content.estimate_data.gyro.print();
		printf(",mag=");
		content.estimate_data.mag.print();
		printf("\n");
		break;
	case posture_static_calib_data:
		printf("posture_static_calib:accl=");
		content.static_calib_data.accl.print();
		printf(",gyro=");
		content.static_calib_data.gyro.print();
		printf(",mag=");
		content.static_calib_data.mag.print();
		printf("g0=");
		content.static_calib_data.g0.print();
		printf(",gyro_offset=");
		content.static_calib_data.gyro_offset.print();
		printf(",mag0=");
		content.static_calib_data.mag0.print();
		printf("\n");
		break;
	case posture_dynamic_calib_data:
		printf("posture_dynamic_calib:mag=");
		content.dynamic_calib_data.mag.print();
		printf(",mag_offset=");
		content.dynamic_calib_data.mag_offset.print();
		printf("\n");
		break;
	}
}

quat posture::cal_pos(const vect&accl,const vect&gyro, const vect&mag){
	//回転ベクトルの座標変換
	vect gyro_absbase=q_pos*quat(gyro)*~q_pos;

	//回転クウォータニオンの計算
	quat q_rot(1-gyro_absbase.abs2()*dt*dt*0.125,gyro_absbase*dt*0.5);

	//姿勢角の変更
	quat q_pos2=q_rot*q_pos;

	//正規化
	q_pos2*=(1.5-0.5*q_pos2.abs2());


	//地磁気による補正
	if(f_enable_mag_correct){

	}

	//加速度による補正
	if(f_enable_accl_correct){
		//絶対座標での加速度を計算
		vect g_absbase=q_pos*quat(accl)*~q_pos;

		//回転ベクトルを計算
		vect v_rot=g_absbase/g0/g0.abs2();
		//printf("rot_v=");
		//v_rot.print();
		//クウォータニオン作成
		quat q_correct(1-v_rot.abs2()*0.125,v_rot*0.5);

		//補正
		q_pos2=q_correct*q_pos2;
	}
	return q_pos2;
}
posture::posture():dt(0),q_pos(1,0,0,0),state(state_estimate),f_enable_accl_correct(true),f_enable_mag_correct(false){
}
bool posture::init(float dt){
	this->dt=dt;
	imu.init();
	//前回保存データを取得
	//g0,gyro_offset,mag_offset
	my_nvs.init();
	esp_err_t err;
	size_t size;
	err=my_nvs.get_blob_data("posture","g0",&g0,&size);
	vect v_temp;
	err=my_nvs.get_blob_data("posture","gyro_offset",&v_temp,&size);
	if(err==ESP_OK)imu.set_gyro_offset(v_temp);
	err=my_nvs.get_blob_data("posture","mag_offset",&v_temp,&size);
	if(err==ESP_OK)imu.set_mag_offset(v_temp);
	else{
		switch(err){
		case ESP_ERR_NVS_NOT_INITIALIZED:
			printf("posture:ESP_ERR_NVS_NOT_INITIALIZED\n");
			break;
		case ESP_FAIL:
			printf("posture:ESP_FAIL\n");
			break;
		case ESP_ERR_NVS_PART_NOT_FOUND:
			printf("posture:ESP_ERR_NVS_PART_NOT_FOUND\n");
			break;
		case ESP_ERR_NVS_NOT_FOUND:
			printf("posture:ESP_ERR_NVS_NOT_FOUND\n" );
			break;
		case ESP_ERR_NVS_INVALID_NAME :
			printf("posture:ESP_ERR_NVS_INVALID_NAME \n" );
			break;
		default:
			printf("posture:other_err\n");
		}
	}
	return 1;
}

void posture::get_data( posture_data_t* data){
	switch(state){
	case state_estimate:
	{
		//フィルター済みデータ取得
		IMU_data_t imu_data;
		imu.get_IMU_data(&imu_data);
		//姿勢推定
		q_pos=cal_pos(imu_data.accl,imu_data.gyro,imu_data.mag);
		//返送データ作成
		data->type=posture_estimate_data;
		data->content.estimate_data.accl=imu_data.accl;
		data->content.estimate_data.gyro=imu_data.gyro;
		data->content.estimate_data.mag=imu_data.mag;
		data->content.estimate_data.q_pos=q_pos;

		break;
	}
	case state_static_calib:
	{
		//9軸データ取得
		imu.get_raw_data(&data->content.static_calib_data.accl,&data->content.static_calib_data.gyro,&data->content.static_calib_data.mag);
		//平均値計算
		data->content.static_calib_data.g0=static_calib_accl_buf.cal(data->content.static_calib_data.accl);
		data->content.static_calib_data.gyro_offset=static_calib_gyro_buf.cal(data->content.static_calib_data.gyro);
		data->content.static_calib_data.mag0=static_calib_mag_buf.cal(data->content.static_calib_data.mag);
		//パラメータ更新
		g0=data->content.static_calib_data.g0;
		imu.set_gyro_offset(data->content.static_calib_data.gyro_offset);
		mag0=data->content.static_calib_data.mag0;
		break;
	}
	case state_dynamic_calib:
	{
		//データ取得
		vect mag;
		imu.get_raw_data(NULL,NULL,&mag);
		//最大、最小ベクトルの作成
		if(f_first_dynamic_calib){
			mag_max=mag_min=mag;
		}
		else{
			for(int i=0;i<3;++i){
				if(mag_max[i]<mag[i])mag_max[i]=mag[i];
				else if(mag_min[i]>mag[i])mag_min[i]=mag[i];
			}
		}
		//出力データ作成
		data->type=posture_dynamic_calib_data;
		data->content.dynamic_calib_data.mag=mag;
		data->content.dynamic_calib_data.mag_offset=(mag_min+mag_max)*0.5;
		//パラメータ更新
		imu.set_mag_offset(data->content.dynamic_calib_data.mag_offset);
		break;
	}
	}
}
void posture::start_estimate(){
	state=state_estimate;
}
void posture::start_static_calib(){
	state=state_static_calib;
}
void posture::start_dynamic_calib(){
	state=state_static_calib;
}
void posture::set_g0(const vect&v){
	g0=v;
}
void posture::set_gyro_offset(const vect&v){
	imu.set_gyro_offset(v);
}
void posture::set_mag0(const vect&v){
	mag0=v;
}
void posture::set_mag_offset(const vect&v){
	imu.set_mag_offset(v);
}
vect posture::get_g0()const{
	return g0;
}
vect posture::get_mag0()const{
	return mag0;
}
vect posture::get_gyro_offset()const{
	return imu.get_gyro_offset();
}
vect posture::get_mag_offset()const{
	return imu.get_mag_offset();
}
void posture::enable_accl_correct(){

}
void posture::disable_accl_correction(){

}
void posture::enable_mag_correct(){

}
void posture::disable_mag_correct(){

}


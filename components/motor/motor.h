/*
 * motor.h
 *
 *  Created on: 5 Aug 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_MOTOR_MOTOR_H_
#define COMPONENTS_MOTOR_MOTOR_H_
extern"C"{
#include"driver/gpio.h"
#include"driver/ledc.h"
}
class motor{
	uint8_t pin;
	ledc_channel_t ledc_channel;
	float duty;
	bool f_initted;
public:
	motor():pin(0),ledc_channel(LEDC_CHANNEL_0),duty(0),f_initted(false){}
	motor(uint8_t pin,ledc_channel_t ledc_channel,float duty);
	void init(uint8_t pin,ledc_channel_t ledc_channel,float duty);
	void set_duty(float duty);
	float get_duty()const;
};



#endif /* COMPONENTS_MOTOR_MOTOR_H_ */

/*
 * motor.cpp
 *
 *  Created on: 5 Aug 2020
 *      Author: yuta2
 */
#include "../motor/motor.h"


#define LEDC_PWM_FREQ 5000
#define LEDC_PWM_RESOLUTION 12
#define LEDC_DUTY_MAX 4095
#define LEDC_TIMER LEDC_TIMER_1


motor::motor(uint8_t pin,ledc_channel_t ledc_channel,float duty):pin(pin),ledc_channel(ledc_channel),duty(duty),f_initted(1){
    gpio_set_direction((gpio_num_t)pin, GPIO_MODE_OUTPUT);

    ledc_timer_config_t tconf;
    tconf.clk_cfg=LEDC_AUTO_CLK;
    tconf.duty_resolution=(ledc_timer_bit_t)LEDC_PWM_RESOLUTION;
    tconf.freq_hz=LEDC_PWM_FREQ;
    tconf.speed_mode=LEDC_HIGH_SPEED_MODE;
    tconf.timer_num=LEDC_TIMER;
    ledc_timer_config(&tconf);

    ledc_channel_config_t cconf;
    cconf.channel=ledc_channel;
    cconf.duty=0;
    cconf.gpio_num=pin;
    cconf.hpoint=0;
    cconf.intr_type=LEDC_INTR_DISABLE;
    cconf.speed_mode=LEDC_HIGH_SPEED_MODE;
    cconf.timer_sel=LEDC_TIMER;
    ledc_channel_config(&cconf);

    set_duty(duty);
}
void motor::init(uint8_t pin,ledc_channel_t ledc_channel,float duty){
	this->duty=duty;
	this->pin=pin;
	this->ledc_channel=ledc_channel;
    gpio_set_direction((gpio_num_t)pin, GPIO_MODE_OUTPUT);

    ledc_timer_config_t tconf;
    tconf.clk_cfg=LEDC_AUTO_CLK;
    tconf.duty_resolution=(ledc_timer_bit_t)LEDC_PWM_RESOLUTION;
    tconf.freq_hz=LEDC_PWM_FREQ;
    tconf.speed_mode=LEDC_HIGH_SPEED_MODE;
    tconf.timer_num=LEDC_TIMER;
    ledc_timer_config(&tconf);

    ledc_channel_config_t cconf;
    cconf.channel=ledc_channel;
    cconf.duty=0;
    cconf.gpio_num=pin;
    cconf.hpoint=0;
    cconf.intr_type=LEDC_INTR_DISABLE;
    cconf.speed_mode=LEDC_HIGH_SPEED_MODE;
    cconf.timer_sel=LEDC_TIMER;
    ledc_channel_config(&cconf);

    set_duty(duty);

}
void motor::set_duty(float duty){
	if(duty<0)duty=0;
	else if(duty>1.0)duty=1.0;
	ledc_set_duty(LEDC_HIGH_SPEED_MODE,ledc_channel,(uint16_t)(duty*(float)LEDC_DUTY_MAX));
	ledc_update_duty(LEDC_HIGH_SPEED_MODE,ledc_channel);
	this->duty=duty;
}
float motor::get_duty()const{return duty;}



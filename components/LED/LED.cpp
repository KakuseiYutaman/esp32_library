/*
 * LED.cpp
 *
 *  Created on: 5 Aug 2020
 *      Author: yuta2
 */
#include"LED.h"


#define LEDC_PWM_FREQ 5000
#define LEDC_PWM_RESOLUTION 12
#define LEDC_DUTY_MAX 4095
#define LEDC_TIMER LEDC_TIMER_0

void led::update_task(void*param){
	while(1){
		led* p_led=(led*)param;
		switch(p_led->ope_type){
		case led_on:
			p_led->set_duty(1.0);
			break;
		case led_off:
			p_led->set_duty(0.0);
			break;
		case led_onoff:
		{
			++p_led->dir;
			if(p_led->dir==5)p_led->set_duty(0.0);
			else if(p_led->dir==10){
				p_led->set_duty(1.0);
				p_led->dir=0;
			}
			break;
		}
		case led_onoff_slow:
		{
			++p_led->dir;
			if(p_led->dir==15)p_led->set_duty(0.0);
			else if(p_led->dir==30){
				p_led->set_duty(1.0);
				p_led->dir=0;
			}
			break;
		}
		case led_onoff_fast:
		{
			++p_led->dir;
			if(p_led->dir==0)p_led->set_duty(0.0);
			else if(p_led->dir==1){
				p_led->set_duty(1.0);
				p_led->dir=0;
			}
			break;
		}
		case led_low:
			p_led->set_duty(0.15);
			break;
		case led_hotaru:
			if(p_led->dir==0){
				p_led->duty+=0.03;
				if(p_led->duty>=1.0){
					p_led->duty=1.0;
					p_led->dir=1;
				}
			}
			else{
				p_led->duty-=0.03;
				if(p_led->duty<=0.0){
					p_led->duty=0.0;
					p_led->dir=0;
				}
			}
			p_led->set_duty(p_led->duty);
			break;

		case led_hotaru_up:
		{
			p_led->duty+=0.05;
			if(p_led->duty>=1.0)p_led->duty=0.0;
			p_led->set_duty(p_led->duty);
			break;
		}
		case led_hotaru_down:
		{
			p_led->duty-=0.05;
			if(p_led->duty<=0.0)p_led->duty=1.0;
			p_led->set_duty(p_led->duty);
			break;
		}
		}
		vTaskDelay(30/portTICK_PERIOD_MS);
	}
}
void led::set_duty(float duty){
	if(duty<0)duty=0;
	else if(duty>1.0)duty=1.0;
	ledc_set_duty(ledc_mode,ledc_channel,(uint16_t)(duty*(float)LEDC_DUTY_MAX));
	ledc_update_duty(ledc_mode,ledc_channel);
	this->duty=duty;
}
led::led(uint8_t pin,ledc_channel_t ledc_channel,ledc_mode_t ledc_mode,led_ope_type_t ope_type):pin(pin),ledc_channel(ledc_channel),ledc_mode(ledc_mode),ope_type(ope_type),duty(0),dir(1),f_initted(1),h_update_task(NULL){
    gpio_set_direction((gpio_num_t)pin, GPIO_MODE_OUTPUT);

    ledc_timer_config_t tconf;
    tconf.clk_cfg=LEDC_AUTO_CLK;
    tconf.duty_resolution=(ledc_timer_bit_t)LEDC_PWM_RESOLUTION;
    tconf.freq_hz=LEDC_PWM_FREQ;
    tconf.speed_mode=ledc_mode;
    tconf.timer_num=(ledc_timer_t)LEDC_TIMER;
    ledc_timer_config(&tconf);

    ledc_channel_config_t cconf;
    cconf.channel=ledc_channel;
    cconf.duty=0;
    cconf.gpio_num=pin;
    cconf.hpoint=0;
    cconf.intr_type=LEDC_INTR_DISABLE;
    cconf.speed_mode=ledc_mode;
    cconf.timer_sel=(ledc_timer_t)LEDC_TIMER;
    ledc_channel_config(&cconf);
    xTaskCreatePinnedToCore(update_task,"led update task",2048,(void*)this,1,&h_update_task,1);
}
void led::init(uint8_t pin,ledc_channel_t ledc_channel,ledc_mode_t ledc_mode,led_ope_type_t ope_type){
	this->pin=pin;
	this->ledc_channel=ledc_channel;
	this->ledc_mode=ledc_mode;
	this->ope_type=ope_type;
	duty=0;
	dir=1;
	f_initted=1;
    gpio_set_direction((gpio_num_t)pin, GPIO_MODE_OUTPUT);

    ledc_timer_config_t tconf;
    tconf.clk_cfg=LEDC_AUTO_CLK;
    tconf.duty_resolution=(ledc_timer_bit_t)LEDC_PWM_RESOLUTION;
    tconf.freq_hz=LEDC_PWM_FREQ;
    tconf.speed_mode=ledc_mode;
    tconf.timer_num=LEDC_TIMER;
    ledc_timer_config(&tconf);

    ledc_channel_config_t cconf;
    cconf.channel=ledc_channel;
    cconf.duty=0;
    cconf.gpio_num=pin;
    cconf.hpoint=0;
    cconf.intr_type=LEDC_INTR_DISABLE;
    cconf.speed_mode=ledc_mode;
    cconf.timer_sel=LEDC_TIMER;
    ledc_channel_config(&cconf);
    xTaskCreatePinnedToCore(update_task,"led update task",2048,(void*)this,1,&h_update_task,1);
}

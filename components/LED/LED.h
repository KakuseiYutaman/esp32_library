/*
 * LED.h
 *
 *  Created on: 5 Aug 2020
 *      Author: yuta2
 */

#ifndef LED_LED_H_
#define LED_LED_H_

extern"C"{
#include "driver/gpio.h"
#include"driver/ledc.h"
#include"freertos/FreeRTOS.h"
#include"freertos/task.h"
}

class led{
public:
	enum led_ope_type_t{
		led_on,
		led_off,
		led_onoff,
		led_onoff_slow,
		led_onoff_fast,
		led_low,
		led_hotaru,
		led_hotaru_up,
		led_hotaru_down,
	};
private:
	uint8_t pin;
	ledc_channel_t ledc_channel;
	ledc_mode_t ledc_mode;
	led_ope_type_t ope_type;
	float duty;
	uint8_t dir;
	bool f_initted;
	TaskHandle_t h_update_task;
	static void update_task(void*param);
	void set_duty(float duty);
public:
	led():pin(0),ledc_channel(LEDC_CHANNEL_0),ledc_mode(LEDC_HIGH_SPEED_MODE),ope_type(led_off),duty(0),dir(1),f_initted(0),h_update_task(NULL){}
	led(uint8_t pin,ledc_channel_t ledc_channel,ledc_mode_t ledc_mode,led_ope_type_t ope_type);
	void init(uint8_t pin,ledc_channel_t ledc_channel,ledc_mode_t ledc_mode,led_ope_type_t ope_type);
	void set_ope_type(led_ope_type_t ope_type){this->ope_type=ope_type;}
};



#endif /* LED_LED_H_ */

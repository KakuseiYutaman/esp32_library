/*
 * MadgwickFiter.h
 *
 *  Created on: 5 Nov 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_MPU9250_MADGWICKFITER_H_
#define COMPONENTS_MPU9250_MADGWICKFITER_H_

#include"quat.h"

class MadgwickFilter{
	float deltat;
	float beta;
	float zeta;
	float SEq_1,SEq_2,SEq_3,SEq_4;//センサーフレームから地球フレームへのクウォータニオン
	float b_x,b_z;//地磁気＠地球フレーム、x軸は北を向いているとする
	float w_bx,w_by,w_bz;
	//加速度センサの値は常に鉛直下向き大きさ1として計算
	quat pos0;//最初の姿勢、姿勢角の出力はこれをもとに行われるべき
public:
	quat pos;//姿勢角
	MadgwickFilter(float dt,float beta,float zeta):deltat(dt),beta(beta),zeta(zeta),SEq_1(1),SEq_2(0),SEq_3(0),SEq_4(0)
	,b_x(1),b_z(0),w_bx(0),w_by(0),w_bz(0),pos0(1,0,0,0),pos(1,0,0,0){}
	quat update(vect accl,vect gyro,vect mag);
	void init_pos(vect accl,vect mag);//地磁気と加速度から姿勢を推定

};



#endif /* COMPONENTS_MPU9250_MADGWICKFITER_H_ */

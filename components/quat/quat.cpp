/*
 * quat.cpp
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */
extern"C"{
#include<string.h>
}

#include"quat.h"

bool quat::str_to_quat(quat*q,const char*q0){
	int v0len=strlen(&q0[0]);
	//かっことカンマの数をチェック
	if(q0[0]!='('||q0[v0len-1]!=')')return 0;
	int n_km=0;
	for(int i=1;i<v0len-1;++i){
		if(q0[i]=='('||q0[i]==')')return 0;
		if(q0[i]==',')++n_km;
	}
	if(n_km!=2)return 0;
	int i_begin_num=1;
	int i_q=0;
	for(int i=1;i<v0len;++i){
		if(q0[i]==','||q0[i]==')'){
			char strnum[256];
			memcpy(strnum,q0+i_begin_num,(i-1)-i_begin_num+1);
			strnum[i-i_begin_num]='\0';
			(*q)[i_q]=atof(strnum);
			i_begin_num=i+1;
			i_q++;
		}
	}
	return 1;
}
quat::quat():r(0),v(0,0,0){}
float quat::abs2()const {
	return r * r + v.abs2();
}
quat::quat(float r, float x, float y, float z) {
	this->r = r;
	v[0] = x;
	v[1] = y;
	v[2] = z;
}
quat::quat(float r, const vect& v) {
	this->r = r;
	this->v = v;
}
quat::quat(const vect& v) {
	r = 0;
	this->v = v;
}
quat::quat(const float* q) {
	r = q[0];
	v[0] = q[1];
	v[1] = q[2];
	v[2] = q[3];
}
quat::quat(const quat&q) {
	r = q.r;
	v = q.v;
}
quat& quat::operator=(const quat&q)& {
	r = q.r;
	v = q.v;
	return*this;
}
quat& quat::operator+=(const quat&q) {
	r += q.r;
	v += q.v;
	return*this;
}
quat& quat::operator-=(const quat& q) {
	r -= q.r;
	v -= q.v;
	return*this;
}
quat& quat::operator*=(const quat&q) {
	*this = quat(r * q.r - v * q.v, r * q.v + q.r * v + v / q.v);
	return*this;
}
quat& quat::operator*=(float a) {
	r *= a;
	v *= a;
	return *this;
}
quat& quat::operator/=(const quat& q) {
	quat p = ~q;
	*this *= p;
	*this /= (q.r * q.r + q.v.abs2());
	return*this;
}
quat& quat::operator/=(float a) {
	r /= a;
	v /= a;
	return*this;
}
quat quat::operator+()const {
	return quat(*this);
}
quat quat::operator-()const {
	return quat(-r, -v);
}
quat quat::operator~()const {
	return quat(r, -v);
}
quat::operator vect()const {
	return v;
}
float quat::operator[](int i)const {
	if (i == 0)return r;
	else return v[i - 1];
}
float& quat::operator[](int i) {
	if (i == 0)return r;
	else return v[i - 1];
}
vect quat::rotate_vect(const vect&v){
	return (*this)*(quat)v*(~(*this));
}

quat operator+(const quat& p, const quat& q) {
	return quat(p)+=q;
}
quat operator-(const quat& p, const quat& q) {
	return quat(p) -= q;
}
quat operator*(const quat& p, const quat& q) {
	return quat(p) *= q;
}
quat operator/(const quat& p, const quat& q) {
	return quat(p) /= q;
}
quat operator*(const quat& p, float a) {
	return quat(p) *= a;
}
quat operator*(float a, const quat& q) {
	return quat(q) *= a;
}
quat operator/(const quat& p, float a) {
	return quat(p) /= a;
}



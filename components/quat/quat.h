/*
 * quat.h
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_QUAT_QUAT_H_
#define COMPONENTS_QUAT_QUAT_H_

#include"vect.h"

class quat {
	float r;
	vect v;
public:
	static bool str_to_quat(quat*,const char*);
	quat();
	quat(float, float, float, float);
	quat(float, const vect&);
	quat(const vect&);
	quat(const float*);
	quat(const quat&);
	quat& operator=(const quat&)&;
	quat& operator+=(const quat&);
	quat& operator-=(const quat&);
	quat& operator*=(const quat&);
	quat& operator*=(float);
	quat& operator/=(const quat&);
	quat& operator/=(float a);
	quat operator+()const;
	quat operator-()const;
	quat operator~()const;
	operator vect()const;
	float operator[](int i)const;
	float& operator[](int i);
	float abs2()const;
	float abs()const{return sqrt(abs2());}
	void print()const{printf("(%f,%f,%f,%f)",r,v[0],v[1],v[2]);}
	vect rotate_vect(const vect&);
};

quat operator+(const quat& p, const quat& q);
quat operator-(const quat& p, const quat& q);
quat operator*(const quat& p, const quat& q);
quat operator/(const quat& p, const quat& q);
quat operator*(const quat& p, float a);
quat operator*(float a, const quat& q);
quat operator/(const quat& p,float a);



#endif /* COMPONENTS_QUAT_QUAT_H_ */

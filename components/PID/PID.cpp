/*
 * PID.cpp
 *
 *  Created on: 17 Aug 2020
 *      Author: yuta2
 */
#include"PID.h"


void pid_buffer::enable_I_limit(){
	f_I_limit=1;
}
void pid_buffer::disable_I_limit(){
	f_I_limit=0;
}
void pid_buffer::set_I_limit(float limit){
	I_limit=limit;
}
float pid_buffer::get_I_limit()const{
	return I_limit;
}
void pid_buffer::set_target(float target){
	this->target=target;
}
float pid_buffer::get_target()const{
	return target;
}
void pid_buffer::set_Kp(float K){
	Kp=K;
}
void pid_buffer::set_Kd(float K){
	Kd=K;
}
void pid_buffer::set_Ki(float K){
	Ki=K;
}
float pid_buffer::get_Kp()const{
	return Kp;
}
float pid_buffer::get_Kd()const{
	return Kd;
}
float pid_buffer::get_Ki()const{
	return Ki;
}
float pid_buffer::P_control(float current_data, float dt){
	float dif=target-current_data;
	I+=dif*dt;
	old_dif=dif;
	return Kp*dif;
}
float pid_buffer::PI_control(float current_data, float dt){
	float dif=target-current_data;
	I+=dif*dt;
	old_dif=dif;
	if(f_I_limit&&I>I_limit)return Kp*dif+Ki*I_limit;
	return Kp*dif+Ki*I;
}
float pid_buffer::PD_control(float current_data, float dt){
	float dif=target-current_data;
	I+=dif*dt;
	float output=Kp*dif+Kd*(dif-old_dif)/dt;
	old_dif=dif;
	return output;
}
float pid_buffer::PD_control(float current_data, float d, float dt){
	float dif=target-current_data;
	I+=dif*dt;
	old_dif=dif;
	return Kp*dif+Kd*d;
}
float pid_buffer::PID_control(float current_data, float dt){
	float dif=target-current_data;
	I+=dif*dt;
	float output;
	if(f_I_limit&&I>I_limit)output=Kp*dif+Kd*(dif-old_dif)/dt+Ki*I_limit;
	else output=Kp*dif+Kd*(dif-old_dif)/dt+Ki*I;
	old_dif=dif;
	return output;
}
float pid_buffer::PID_control(float current_data, float d, float dt){
	float dif=target-current_data;
	I+=dif*dt;
	if(f_I_limit&&I>I_limit)return Kp*dif+Kd*dt+Ki*I_limit;
	return Kp*dif+Kd*d+Ki*I;
}




/*
 * PID.h
 *
 *  Created on: 17 Aug 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_PID_PID_H_
#define COMPONENTS_PID_PID_H_

class pid_buffer{
	bool f_I_limit;
	float target, I, I_limit, old_dif;
	float Kp, Kd, Ki;
public:
	pid_buffer():f_I_limit(0),target(0),I(0),I_limit(0),old_dif(0),Kp(0),Kd(0),Ki(0){}
	void enable_I_limit();
	void disable_I_limit();
	void set_I_limit(float);
	float get_I_limit()const;
	void set_target(float);
	float get_target()const;
	void set_Kp(float);
	void set_Kd(float);
	void set_Ki(float);
	float get_Kp()const;
	float get_Kd()const;
	float get_Ki()const;
	float P_control(float current_data, float dt);
	float PI_control(float current_data, float dt);
	float PD_control(float current_data, float dt);
	float PD_control(float current_data, float d, float dt);
	float PID_control(float current_data, float dt);
	float PID_control(float current_data, float d, float dt);
};



#endif /* COMPONENTS_PID_PID_H_ */

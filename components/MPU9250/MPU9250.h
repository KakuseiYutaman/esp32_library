/*
 * MPU9250.h
 *
 *  Created on: 9 Sep 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_MPU9250_MPU9250_H_
#define COMPONENTS_MPU9250_MPU9250_H_

#include"quat.h"
extern "C"{
#include"driver/spi_master.h"
#include"freertos/FreeRTOS.h"
#include"freertos/Task.h"
#include<string.h>
}


class MPU9250{
public:
	enum accl_range_t{
		accl_range_2g,
		accl_range_4g,
		accl_range_8g,
		accl_range_16g,
	};
	enum gyro_range_t{
		gyro_range_250dps,
		gyro_range_500dps,
		gyro_range_1000dps,
		gyro_range_2000dps,
	};
	enum mag_resolution_t{
		mag_resolution_14bit,//=0.6uT/LSB
		mag_resolution_16bit,//=0.15uT/LSB
	};
	enum state_t{
		state_init,
		state_measure,
		state_static_calib,//マグウィックフィルタかけるならたぶんジャイロのオフセットさえ設定できればOK
		state_dynamic_calib,//地磁気の中心と半径を計算
	};
	enum flying_mag_calib_t{
		flying_mag_calib_enable,
		flying_mag_calib_disable,
	};
private:
	portMUX_TYPE mux_data;
	spi_host_device_t spi_host;//デバイスは中でいい感じにするからバスは外でいい感じにしてくれや
	int spi_freq;//読み取り用。書き込み時はいい感じにしてあげる。
	uint8_t spi_cs_pin;
	accl_range_t accl_range;//accl_range_tから選んで
	gyro_range_t gyro_range;//gyro_range_tから選んで
	mag_resolution_t mag_resolution;//mag_resolution_tから選択
	bool enable_interrupt;
	uint8_t int_pin;
	float dt;
	vect gyro_offset,mag_offset;

	TaskHandle_t int_task;
	TaskHandle_t h_mpu9250_task;
	void internal_update();
	static void IRAM_ATTR int_pin_isr_handler(void*);
	static void mpu9250_task(void*);
public:
	vect accl,gyro,mag;
	MPU9250(spi_host_device_t spi_host,uint8_t spi_cs_pin,accl_range_t accl_range,gyro_range_t gyro_range,mag_resolution_t mag_resolution,int int_pin,float dt);
	float get_dt()const{return dt;}
	void register_interrupt_notify(TaskHandle_t task){int_task=task;};
	void update_data();
	void start_static_calib();
	void finish_static_calib();

};


#endif /* COMPONENTS_MPU9250_MPU9250_H_ */

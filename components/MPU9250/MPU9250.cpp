/*
 * MPU9250.cpp
 *
 *  Created on: 9 Sep 2020
 *      Author: yuta2
 */

#include"MPU9250.h"
#include"MPU9250_REGISTER_MAP.h"
#include"nvs_flash_class.h"
#include"VMA.h"

#define MAX_DATA_SIZE 30
extern "C"{
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include"driver/gpio.h"
}

//#define SELF_TEST

static spi_device_handle_t spi_r;//読み取り用と書き込み用
uint8_t rx_buf[MAX_DATA_SIZE];
uint8_t tx_buf[MAX_DATA_SIZE];
spi_transaction_t tr;

float accl_max,gyro_max,mag_unit=0.15;

static void MPU9250_read_bytes(uint8_t addr, uint8_t len);//read_flagは中でつける
static uint8_t MPU9250_read_byte(uint8_t addr){
	MPU9250_read_bytes(addr,1);
	return rx_buf[0];
}
static void MPU9250_write_bytes(uint8_t addr, uint8_t len);
static void MPU9250_write_byte(uint8_t addr, uint8_t data){
	tx_buf[0]=data;
	MPU9250_write_bytes(addr,1);
	vTaskDelay(1/portTICK_PERIOD_MS);
	//if(MPU9250_read_byte(addr)!=data){
		//printf("faile writing to 0x%x\n",addr);
	//}
}

MPU9250::MPU9250(spi_host_device_t spi_host,uint8_t spi_cs_pin,accl_range_t accl_range,gyro_range_t gyro_range,
		mag_resolution_t mag_resolution,int int_pin,float dt)
:spi_host(spi_host),spi_freq(500000),spi_cs_pin(spi_cs_pin),accl_range(accl_range),gyro_range(gyro_range),
 mag_resolution(mag_resolution),enable_interrupt(0),int_pin(int_pin),dt(dt),int_task(NULL),h_mpu9250_task(NULL){
	if(int_pin<0)enable_interrupt=0;
	else enable_interrupt=1;

    spi_device_interface_config_t devconf;
	memset(&devconf, 0, sizeof(devconf));
    devconf.clock_speed_hz=spi_freq;
    devconf.mode=3;
    devconf.spics_io_num=spi_cs_pin;
    devconf.address_bits=8;
    devconf.queue_size=7;//	てきとー
    esp_err_t ret=spi_bus_add_device(HSPI_HOST, &devconf, &spi_r);
    ESP_ERROR_CHECK(ret);
    printf("finish add read device\n");

    if(MPU9250_read_byte(0x75)!=0x71){
    	printf("No mpu9250 on the spi bus\n");
    }
    printf("mpu9250 connected\n");

    //割り込み設定
    //INT_PIN:ActiveHigh&PP&output 50us pulse, 全ての読み取りで割り込みクリア, FSYNCピン割り込み無効,バイパス無効
    MPU9250_write_byte(INT_PIN_CFG,0b0000000);
    //データレディ割り込みだけ
    MPU9250_write_byte(INT_ENABLE,0x01);
    //sample_rate=internal_Fs(1kHz)/100=10Hz
    MPU9250_write_byte(SMPLRT_DIV,99);
    //内部サンプルレート1kHz,ジャイロバンド帯92Hz//Fchoice_bはあとでちゃんと有効かすること
    MPU9250_write_byte(MPU_CONFIG,0x02);
    //加速度サンプルレート1kHz,バンド帯92Hz
    MPU9250_write_byte(ACCEL_CONFIG2,0x08|0x02);
    //FIFO無効
    MPU9250_write_byte(FIFO_EN,0x00);
    //FIFO無効,I2Cマスタ有効,I2C_SLAVE機能無効,I2Cインターフェース無効,各種リセットなし
    MPU9250_write_byte(USER_CTRL,0x30);
    //全センサ全軸有効化
    MPU9250_write_byte(0x6C,0x00);


    //range setting
    MPU9250_read_bytes(ACCEL_CONFIG,1);
    uint8_t setted_accl_rang=(rx_buf[0]&0x18)>>3;
    int range_n=2;
    for(int i=0;i<setted_accl_rang;++i)range_n*=range_n;
    printf("accl_range=%dg\n",range_n);
    if(setted_accl_rang!=accl_range){
    	range_n=2;
    	for(int i=0;i<accl_range;++i)range_n*=range_n;
    	printf("change accl_range to %dg\n",range_n);
    	MPU9250_write_byte(ACCEL_CONFIG,((uint8_t)accl_range)<<3);
    }
    accl_max=(float)range_n;

    MPU9250_read_bytes(GYRO_CONFIG,1);
    uint8_t setted_gyro_range=(rx_buf[0]&0x18)>>3;
    gyro_max=250.0f*3.141592/180.0f;
    for(int i=0;i<setted_gyro_range;++i)gyro_max*=2.0f;
    printf("gyro_range=%f[rad/s]\n",gyro_max);
    if(setted_gyro_range!=gyro_range||((rx_buf[0]&0x03)!=0x03)){
        gyro_max=250.0f*3.141592/180.0f;
        for(int i=0;i<gyro_range;++i)gyro_max*=2.0f;
        printf("change gyro_range to %f\n",gyro_max);
        MPU9250_write_byte(GYRO_CONFIG,(((uint8_t)gyro_range)<<3));
    }
    //I2C設定
    MPU9250_write_byte(I2C_MST_CTRL,0x4D);//外部センサーロード待ち有効,SCL=400kHz
    //I2C_SLV0設定
    MPU9250_write_byte(I2C_SLV0_ADDR,0x0c);
    MPU9250_write_byte(I2C_SLV0_REG,0x0A);
    MPU9250_write_byte(I2C_SLV0_DO,0x16);//16bit連続測定100Hz
    MPU9250_write_byte(I2C_SLV0_CTRL,0x98);//SLV0有効
    MPU9250_read_byte(INT_STATUS);
    while(!(MPU9250_read_byte(INT_STATUS)&0x01)){
    	vTaskDelay(1);
    }
    MPU9250_write_byte(I2C_SLV0_REG,0x00);
    MPU9250_write_byte(I2C_SLV0_DO,0xff);//これないとなんか書き込みがストップしない
    while(!(MPU9250_read_byte(INT_STATUS)&0x01)){
    	vTaskDelay(1);
    }
    //I2CMasterReset
    MPU9250_write_byte(USER_CTRL,0x36);
    //I2C_SLV0設定
    MPU9250_write_byte(I2C_SLV0_ADDR,0x0c|0x80);
    MPU9250_write_byte(I2C_SLV0_REG,0x02);//ST1から読み出し
    MPU9250_write_byte(I2C_SLV0_CTRL,0x98);//SLV0有効
    while(!(MPU9250_read_byte(INT_STATUS)&0x01)){
    	vTaskDelay(1);
    }

    //sample_rate=internal_Fs(1kHz)/(n+1)=1/dt -> n=1000dt-1
    int n=1000*dt-1;
    if(n>255)n=255;
    MPU9250_write_byte(SMPLRT_DIV,(uint8_t)n);
    dt=(n+1)/1000;

    if(enable_interrupt){
    	xTaskCreatePinnedToCore(mpu9250_task,"mpu_task",2048,this,15,&h_mpu9250_task,1);
    	//Init GPIO
    	gpio_config_t io_conf;
    	io_conf.intr_type=GPIO_INTR_POSEDGE;
    	io_conf.mode=GPIO_MODE_INPUT;
    	io_conf.pin_bit_mask=(1UL<<int_pin);
    	io_conf.pull_down_en=GPIO_PULLDOWN_DISABLE;
    	io_conf.pull_up_en=GPIO_PULLUP_DISABLE;
    	gpio_config(&io_conf);
    	gpio_install_isr_service(0);
    	//task prepare
    	gpio_isr_handler_add((gpio_num_t)int_pin,int_pin_isr_handler,(void*)this);

    }
}
void MPU9250::internal_update(){
	MPU9250_read_bytes(0x3b,14+8);
	int accl_raw_data[3];
	for(uint8_t i=0;i<3;++i){
		accl_raw_data[i]=(int)((((uint16_t)rx_buf[2*i])<<8)+rx_buf[i*2+1]);
		if(accl_raw_data[i]>32767)accl_raw_data[i]-=65536;
		accl[i]=(float)accl_raw_data[i]/32767.0f*accl_max;
	}
	int gyro_raw_data[3];
	for(uint8_t i=0;i<3;++i){
		gyro_raw_data[i]=(int)((((uint16_t)rx_buf[2*i+8])<<8)+rx_buf[i*2+9]);
		if(gyro_raw_data[i]>32767)gyro_raw_data[i]-=65536;
		gyro[i]=(float)gyro_raw_data[i]/32767.0f*gyro_max;
	}
	int mag_raw_data[3];
	for(uint8_t i=0;i<3;++i){
		mag_raw_data[i]=(int)((((uint16_t)rx_buf[2*i+16])<<8)+rx_buf[i*2+15]);
		if(mag_raw_data[i]>32767)mag_raw_data[i]-=65536;
		mag[i]=(float)mag_raw_data[i];
	}
}
void MPU9250::update_data(){
	if(enable_interrupt)return;
	internal_update();
}
void IRAM_ATTR MPU9250::int_pin_isr_handler(void*param){
	xTaskNotifyFromISR(((MPU9250*)param)->h_mpu9250_task,0,eNoAction,NULL);
}
void MPU9250::mpu9250_task(void*param){
	while(1){
		xTaskNotifyWait(0,0,NULL,portMAX_DELAY);
		((MPU9250*)param)->internal_update();
		static int ppp=0;
		printf("%d\n",++ppp);
	}
}
TaskHandle_t h_mpu9250_task;

static void MPU9250_read_bytes(uint8_t addr, uint8_t len){
	memset(&tr,0,sizeof(tr));
	tr.rx_buffer=rx_buf;
	tr.tx_buffer=NULL;
	tr.addr=addr|0x80;
	tr.length=8*len;
	tr.rxlength=8*len;
    assert(spi_device_polling_transmit(spi_r, &tr)==ESP_OK);            // Should have had no issues.
}
static void MPU9250_write_bytes(uint8_t addr, uint8_t len){
	memset(&tr,0,sizeof(tr));
	tr.rx_buffer=NULL;
	tr.tx_buffer=tx_buf;
	tr.addr=addr;
	tr.length=8*len;
    assert(spi_device_polling_transmit(spi_r, &tr)==ESP_OK);            // Should have had no issues.
}


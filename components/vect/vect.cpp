/*
 * vect.cpp
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */

#include"vect.h"
#include<string.h>

bool vect::str_to_vect(vect*v,const char*v0){
	int v0len=strlen(v0);
	//かっことカンマの数をチェック
	if(v0[0]!='('||v0[v0len-1]!=')')return 0;
	int n_km=0;
	for(int i=1;i<v0len-1;++i){
		if(v0[i]=='('||v0[i]==')')return 0;
		if(v0[i]==',')++n_km;
	}
	if(n_km!=2)return 0;
	int i_begin_num=1;
	int i_v=0;
	for(int i=1;i<v0len;++i){
		if(v0[i]==','||v0[i]==')'){
			char strnum[256];
			memcpy(strnum,v0+i_begin_num,(i-1)-i_begin_num+1);
			strnum[i-i_begin_num]='\0';
			(*v)[i_v]=atof(strnum);
			i_begin_num=i+1;
			i_v++;
		}
	}
#ifdef TEST_STR_CONVERSION
	printf("%s->",v0);
	vect_print(v);
	printf("\n");

#endif
	return 1;
}
vect::vect() {}
vect::vect(float x0, float x1, float x2) {
	x[0] = x0;
	x[1] = x1;
	x[2] = x2;
}
vect::vect(const vect& a) {
	for (int i = 0; i < 3; ++i)x[i] = a.x[i];
}

vect& vect::operator=(const vect& a)& {
	for (int i = 0; i < 3; ++i)x[i] = a.x[i];
	return*this;
}
vect& vect::operator+=(const vect& a) {
	for (int i = 0; i < 3; ++i)x[i] += a.x[i];
	return*this;
}
vect& vect::operator-=(const vect& a) {
	for (int i = 0; i < 3; ++i)x[i] -= a.x[i];
	return*this;
}
vect& vect::operator*=(float a) {
	for (int i = 0; i < 3; ++i)x[i] *= a;
	return*this;
}
vect& vect::operator/=(const vect& a) {
	(*this) = vect(x[1] * a[2] - x[2] * a[1], x[2] * a[0] - x[0] * a[2], x[0] * a[1] - x[1] * a[0]);
	return*this;
}
vect& vect::operator/=(float a) {
	for (int i = 0; i < 3; ++i)x[i] /= a;
	return*this;
}
float vect::abs2()const {
	float r2 = 0;
	for (int i = 0; i < 3; ++i)r2 += x[i] * x[i];
	return r2;
}
vect vect::operator-()const {
	return vect(-x[0], -x[1], -x[2]);
}
vect vect::operator+()const {
	return vect(*this);
}
float vect::operator[](int i)const {
	return x[i];
}
float& vect::operator[](int i) {
	return x[i];
}
void vect::print()const{printf("(%f,%f,%f)",x[0],x[1],x[2]);}

vect operator+(const vect& a, const vect& b) { return (vect(a) += b); }
vect operator-(const vect& a, const vect& b) { return (vect(a) -= b); }
float operator*(const vect& a, const vect& b) {
	float p = 0;
	for (int i = 0; i < 3; ++i)p += a[i] * b[i];
	return p;
}
vect operator*(float a, const vect& b) { return vect(b) *= a; }
vect operator*(const vect& a, float b) { return (vect(a) *= b); }
vect operator/(const vect& a, const vect& b) { return (vect(a) /= b); }
vect operator/(const vect& a, float b) { return (vect(a) /= b); }




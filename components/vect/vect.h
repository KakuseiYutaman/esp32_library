/*
 * vect.h
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_VECT_VECT_H_
#define COMPONENTS_VECT_VECT_H_

#include<math.h>
#include<stdio.h>

class vect {
	float x[3];
public:
	static bool str_to_vect(vect*,const char*);
	vect();
	vect(float, float, float);
	vect(const float*p_v){x[0]=p_v[0];x[1]=p_v[1];x[2]=p_v[2];}
	vect(const vect&);
	vect& operator=(const vect&)&;
	vect& operator+=(const vect&);
	vect& operator-=(const vect&);
	vect& operator*=(float);
	vect& operator/=(const vect&);//外積
	vect& operator/=(float);
	float abs2()const;//絶対値の2乗
	float abs()const{return sqrt(abs2());}
	float r()const{return sqrt(abs2());}
	vect operator-()const;
	vect operator+()const;
	float operator[](int i)const;
	float& operator[](int i);
	void print()const;
};

vect operator+(const vect&, const vect&);
vect operator-(const vect&, const vect&);
float operator*(const vect&, const vect&);
vect operator*(float, const vect&);
vect operator*(const vect&, float);
vect operator/(const vect&, const vect&);
vect operator/(const vect&, float);

#endif /* INC_VECT_H_ */


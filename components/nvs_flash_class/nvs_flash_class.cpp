/*
 * nvs_flash.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: yuta2
 */
#include"nvs_flash_class.h"

esp_err_t nvs_flash_class::get_uint8_t_data(const char* name_space,const char* key,uint8_t*p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_get_u8(h_nvs,key,p_data);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_uint8_t_data(const char* name_space,const char* key,uint8_t p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_u8(h_nvs,key,p_data);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::get_uint16_t_data(const char* name_space,const char* key,uint16_t*p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_get_u16(h_nvs,key,p_data);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_uint16_t_data(const char* name_space,const char* key,uint16_t p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_u16(h_nvs,key,p_data);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::get_uint32_t_data(const char* name_space,const char* key,uint32_t*p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_get_u32(h_nvs,key,p_data);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_uint32_t_data(const char* name_space,const char* key,uint32_t p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_u32(h_nvs,key,p_data);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::get_uint64_t_data(const char* name_space,const char* key,uint64_t*p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_get_u64(h_nvs,key,p_data);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_uint64_t_data(const char* name_space,const char* key,uint64_t p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_u64(h_nvs,key,p_data);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::get_blob_data(const char*name_space,const char*key,void*p_data,size_t*p_size){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK){
		printf("open_fail\n");
		return err;
	}
	err=nvs_get_blob(h_nvs,key,NULL,p_size);
	if(err==ESP_OK){
		err=nvs_get_blob(h_nvs,key,p_data,p_size);
	}
	else{
		printf("getting size fail\n");
	}
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_blob_data(const char*name_space,const char*key,void*p_data,size_t size){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_blob(h_nvs,key,p_data,size);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}

esp_err_t nvs_flash_class::get_int8_t_data(const char* name_space,const char* key,int8_t*p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_get_i8(h_nvs,key,p_data);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_int8_t_data(const char* name_space,const char* key,int8_t p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_i8(h_nvs,key,p_data);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::get_int16_t_data(const char* name_space,const char* key,int16_t*p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_get_i16(h_nvs,key,p_data);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_int16_t_data(const char* name_space,const char* key,int16_t p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_i16(h_nvs,key,p_data);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::get_int32_t_data(const char* name_space,const char* key,int32_t*p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_get_i32(h_nvs,key,p_data);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_int32_t_data(const char* name_space,const char* key,int32_t p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_i32(h_nvs,key,p_data);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::get_int64_t_data(const char* name_space,const char* key,int64_t*p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READONLY,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_get_i64(h_nvs,key,p_data);
	nvs_close(h_nvs);
	return err;
}
esp_err_t nvs_flash_class::set_int64_t_data(const char* name_space,const char* key,int64_t p_data){
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_set_i64(h_nvs,key,p_data);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}

esp_err_t nvs_flash_class::erase(const char*name_space,const char*key){
	if(key==NULL){
		nvs_handle_t h_nvs;
		esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
		if(err!=ESP_OK)return err;
		err=nvs_erase_all(h_nvs);
		nvs_commit(h_nvs);
		nvs_close(h_nvs);
		return err;
	}
	nvs_handle_t h_nvs;
	esp_err_t err=nvs_open(name_space,NVS_READWRITE,&h_nvs);
	if(err!=ESP_OK)return err;
	err=nvs_erase_key(h_nvs,key);
	nvs_commit(h_nvs);
	nvs_close(h_nvs);
	return err;
}

nvs_flash_class my_nvs;


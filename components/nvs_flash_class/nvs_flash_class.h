/*
 * nvs_flash.h
 *
 *  Created on: 6 Aug 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_NVS_FLASH_CLASS_NVS_FLASH_CLASS_H_
#define COMPONENTS_NVS_FLASH_CLASS_NVS_FLASH_CLASS_H_
extern"C"{
#include"nvs.h"
#include"nvs_flash.h"
}



class nvs_flash_class{
	bool f_initted;
public:
	nvs_flash_class():f_initted(0){}
	void init(){if(!f_initted)nvs_flash_init();f_initted=1;}
	esp_err_t get_uint8_t_data(const char* name_space,const char* key,uint8_t*p_data);
	esp_err_t set_uint8_t_data(const char* name_space,const char* key,uint8_t p_data);
	esp_err_t get_uint16_t_data(const char* name_space,const char* key,uint16_t*p_data);
	esp_err_t set_uint16_t_data(const char* name_space,const char* key,uint16_t p_data);
	esp_err_t get_uint32_t_data(const char* name_space,const char* key,uint32_t*p_data);
	esp_err_t set_uint32_t_data(const char* name_space,const char* key,uint32_t p_data);
	esp_err_t get_uint64_t_data(const char* name_space,const char* key,uint64_t*p_data);
	esp_err_t set_uint64_t_data(const char* name_space,const char* key,uint64_t p_data);
	esp_err_t get_int8_t_data(const char* name_space,const char* key,int8_t*p_data);
	esp_err_t set_int8_t_data(const char* name_space,const char* key,int8_t p_data);
	esp_err_t get_int16_t_data(const char* name_space,const char* key,int16_t*p_data);
	esp_err_t set_int16_t_data(const char* name_space,const char* key,int16_t p_data);
	esp_err_t get_int32_t_data(const char* name_space,const char* key,int32_t*p_data);
	esp_err_t set_int32_t_data(const char* name_space,const char* key,int32_t p_data);
	esp_err_t get_int64_t_data(const char* name_space,const char* key,int64_t*p_data);
	esp_err_t set_int64_t_data(const char* name_space,const char* key,int64_t p_data);
	esp_err_t get_blob_data(const char*name_space,const char*key,void*p_data,size_t*p_size);
	esp_err_t set_blob_data(const char*name_space,const char*key,void*p_data,size_t p_size);
	esp_err_t erase(const char*name_space,const char*key);
};

extern nvs_flash_class my_nvs;


#endif /* COMPONENTS_NVS_FLASH_CLASS_NVS_FLASH_CLASS_H_ */

/*
 * TimerDriver.h
 *
 *  Created on: 4 Aug 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_TIMERDRIVER_TIMERDRIVER_H_
#define COMPONENTS_TIMERDRIVER_TIMERDRIVER_H_


extern "C"{
#include"driver/timer.h"
#include "freertos/FreeRTOS.h"
#include"freertos/task.h"
}

#define configUSE_TASK_NOTIFICATIONS 1

extern float dt;
void timerdriver_init(float dt, TaskHandle_t th);
void timerdriver_stop();
void timerdriver_resume();

#endif /* COMPONENTS_TIMERDRIVER_TIMERDRIVER_H_ */

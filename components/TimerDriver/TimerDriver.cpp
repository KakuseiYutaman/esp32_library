/*
 * TimerDriver.cpp
 *
 *  Created on: 4 Aug 2020
 *      Author: yuta2
 */



extern "C"{
#include <stdio.h>
#include "esp_types.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
}
#include"TimerDriver.h"

#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds

float dt;
int timerdriver_timer_idx;
/*
 * A sample structure to pass events
 * from the timer interrupt handler to the main program.
 */
TaskHandle_t h_notifydest;
/*
 * Timer group0 ISR handler
 *
 * Note:
 * We don't call the timer API here because they are not declared with IRAM_ATTR.
 * If we're okay with the timer irq not being serviced while SPI flash cache is disabled,
 * we can allocate this interrupt without the ESP_INTR_FLAG_IRAM flag and use the normal API.
 */
void IRAM_ATTR timer_group0_isr(void *para){
    int timer_idx = (int) para;

    /* Retrieve the interrupt status and the counter value
       from the timer that reported the interrupt */
    uint32_t intr_status = TIMERG0.int_st_timers.val;
    TIMERG0.hw_timer[timer_idx].update = 1;
    /* Clear the interrupt
       and update the alarm time for the timer with without reload */
    if ((intr_status & BIT(timer_idx)) && timer_idx == timerdriver_timer_idx) {
        TIMERG0.int_clr_timers.t1 = 1;
        xTaskNotifyFromISR(h_notifydest,0,eNoAction,NULL);
    }

    /* After the alarm has been triggered
      we need enable it again, so it is triggered the next time */
    TIMERG0.hw_timer[timer_idx].config.alarm_en = TIMER_ALARM_EN;
}

/*
 * Initialize selected timer of the timer group 0
 *
 * timer_idx - the timer number to initialize
 * auto_reload - should the timer auto reload on alarm?
 * timer_interval_sec - the interval of alarm to set
 */
static void example_tg0_timer_init(int timer_idx,
    bool auto_reload, double timer_interval_sec)
{
	timerdriver_timer_idx=timer_idx;
    /* Select and initialize basic parameters of the timer */
    timer_config_t config;
    config.divider = TIMER_DIVIDER;
    config.counter_dir = TIMER_COUNT_UP;
    config.counter_en = TIMER_PAUSE;
    config.alarm_en = TIMER_ALARM_EN;
    config.intr_type = TIMER_INTR_LEVEL;
    config.auto_reload = auto_reload;
    timer_init(TIMER_GROUP_0, (timer_idx_t)timer_idx, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(TIMER_GROUP_0, (timer_idx_t)timer_idx, 0x00000000ULL);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(TIMER_GROUP_0, (timer_idx_t)timer_idx, timer_interval_sec * TIMER_SCALE);
    uint64_t alarm_val;
    timer_get_alarm_value(TIMER_GROUP_0, (timer_idx_t)timer_idx,&alarm_val);
    dt=(float)((double)alarm_val/TIMER_SCALE);
    timer_enable_intr(TIMER_GROUP_0, (timer_idx_t)timer_idx);
    timer_isr_register(TIMER_GROUP_0, (timer_idx_t)timer_idx, timer_group0_isr,
        (void *) timer_idx, ESP_INTR_FLAG_IRAM, NULL);

    timer_start(TIMER_GROUP_0, (timer_idx_t)timer_idx);
}

/*
 * In this example, we will test hardware timer0 and timer1 of timer group0.
 */

void timerdriver_init(float dt0, TaskHandle_t th)
{
	dt=dt0;
	h_notifydest=th;
    example_tg0_timer_init(TIMER_1, 1, dt);
}

void timerdriver_stop(){
	timer_pause(TIMER_GROUP_0,(timer_idx_t)timerdriver_timer_idx);
}
void timerdriver_resume(){
	timer_start(TIMER_GROUP_0,(timer_idx_t)timerdriver_timer_idx);
}


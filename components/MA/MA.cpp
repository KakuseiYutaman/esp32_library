/*
 * MA.cpp
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */
#include"MA.h"
#include<stdio.h>
#include<stdlib.h>


MA::MA(unsigned int size):size(size),i_buffer(0),f_overflow(false){
	if(size){
		buffer=(float*)malloc(size*sizeof(float));
		if(buffer==NULL){
			printf("malloc err@MA\n");
			size=0;
		}
	}
	else buffer=NULL;
}
MA::~MA(){
	if(buffer)free(buffer);
}
void MA::init(unsigned int size){
	if(size){
		if(buffer)free(buffer);
		buffer=(float*)malloc(size*sizeof(float));
		if(buffer==NULL){
			printf("malloc err@MA\n");
			size=0;
		}
	}
	else buffer=NULL;
}
void MA::add_data(float new_data){
	if(!size)return;
	buffer[i_buffer]=new_data;
	++i_buffer;
	if(i_buffer==size){
		i_buffer=0;
		f_overflow=true;
	}
}
float MA::cal()const{
	if(!size)return 0;
	float sum=0;
	if(f_overflow){
		for(int i=0;i<size;++i)sum+=buffer[i];
		return sum/size;
	}
	else{
		for(int i=0;i<i_buffer;++i)sum+=buffer[i];
		return sum/i_buffer;
	}
}
float MA::cal(float new_data){
	add_data(new_data);
	return cal();
}


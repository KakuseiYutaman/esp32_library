/*
 * MA.h
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_MA_MA_H_
#define COMPONENTS_MA_MA_H_

#include<stdlib.h>

class MA{
	unsigned int size;
	unsigned int i_buffer;
	float *buffer;
	bool f_overflow;
public:
	MA(unsigned int size);
	~MA();
	void init(unsigned int size);
	void add_data(float new_data);
	float cal()const;
	float cal(float new_data);
	unsigned int get_size()const{return size;}
};



#endif /* COMPONENTS_MA_MA_H_ */

/*
 * VMA.h
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_VMA_VMA_H_
#define COMPONENTS_VMA_VMA_H_

#include"vect.h"
#include"MA.h"

class VMA{
	MA v_buf[3];
public:
	VMA(unsigned int size);
	void init(unsigned int size);
	void add_data(const vect& new_data);
	vect cal()const;
	vect cal(const vect&new_data);
	unsigned int get_size()const{return v_buf[0].get_size();}
};


#endif /* COMPONENTS_VMA_VMA_H_ */

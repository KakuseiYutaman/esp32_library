/*
 * VMA.cpp
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */

#include"VMA.h"

VMA::VMA(unsigned int size):v_buf({MA(size),MA(size),MA(size)}){}
void VMA::init(unsigned int size){
	for(int i=0;i<3;++i)v_buf[i].init(size);
}
void VMA::add_data(const vect& new_data){
	for(int i=0;i<3;++i)v_buf[i].add_data(new_data[i]);
}
vect VMA::cal()const{
	return vect(v_buf[0].cal(),v_buf[1].cal(),v_buf[2].cal());
}
vect VMA::cal(const vect&new_data){
	for(int i=0;i<3;++i)v_buf[i].add_data(new_data[i]);
	return vect(v_buf[0].cal(),v_buf[1].cal(),v_buf[2].cal());
}



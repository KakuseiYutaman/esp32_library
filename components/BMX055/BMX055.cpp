/*
 * BMX055.cpp
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */


#include"BMX055.h"
#include"I2C.h"
#include<stdio.h>


#define ACCL_ADDR 0x19
#define GYRO_ADDR 0x69
#define  MAG_ADDR 0x13

BMX055 imu;

static I2C port;
static void accl_write_byte(uint8_t reg,uint8_t data){
	uint8_t ppp[2]={reg,data};
	port.master_write(ppp,2,ACCL_ADDR);
}
static uint8_t accl_read_byte(uint8_t reg){
	uint8_t ppp[1]={reg};
	port.master_write(ppp,1,ACCL_ADDR);
	port.master_read(ppp,1,ACCL_ADDR);
	return ppp[0];
}
static void gyro_write_byte(uint8_t reg,uint8_t data){
	uint8_t ppp[2]={reg,data};
	port.master_write(ppp,2,GYRO_ADDR);
}
static uint8_t gyro_read_byte(uint8_t reg){
	uint8_t ppp[1]={reg};
	port.master_write(ppp,1,GYRO_ADDR);
	port.master_read(ppp,1,GYRO_ADDR);
	return ppp[0];
}
static void mag_write_byte(uint8_t reg,uint8_t data){
	uint8_t ppp[2]={reg,data};
	port.master_write(ppp,2,MAG_ADDR);
}
static uint8_t mag_read_byte(uint8_t reg){
	uint8_t ppp[1]={reg};
	port.master_write(ppp,1,MAG_ADDR);
	port.master_read(ppp,1,MAG_ADDR);
	return ppp[0];
}

BMX055::BMX055():accl_buf(0),gyro_buf(0),mag_buf(0),f_enable_accl_buf(0),f_enable_gyro_buf(0),f_enable_mag_buf(0){}
void BMX055::init(IMU_config_t* conf){
	if(conf){
		if(conf->accl_ma_size){
			accl_buf.init(conf->accl_ma_size);
			f_enable_accl_buf=1;
		}
		if(conf->gyro_ma_size){
			gyro_buf.init(conf->gyro_ma_size);
			f_enable_gyro_buf=1;
		}
		if(conf->mag_ma_size){
			mag_buf.init(conf->mag_ma_size);
			f_enable_mag_buf=1;
		}
	}
	printf("BMX055 port init\n");
	port.init(GPIO_NUM_21,GPIO_NUM_22,400000);
	printf("BMX055 port init finish\n");
	//init accl
	accl_write_byte(0x0F,0x03);//+-2g
	vTaskDelay(1/portTICK_PERIOD_MS);
	accl_write_byte(0x10,0x0C);//set bandwidth 62.5Hz
	vTaskDelay(1/portTICK_PERIOD_MS);
	//init gyro
	gyro_write_byte(0x10,0x04);//Set Filter BandWidth 23Hz,ODR=200Hz(ODR:(probably)Output Data Rate)
	vTaskDelay(1/portTICK_PERIOD_MS);
	gyro_write_byte(0x0F,0x02);//Set Range +-500deg/s(probably drone can't roll faster than 360deg/s)
	vTaskDelay(1/portTICK_PERIOD_MS);
	//Initialize Magnetometer
	mag_write_byte(0x4B,0x83);//soft reset
	vTaskDelay(1/portTICK_PERIOD_MS);
	mag_write_byte(0x4B,0x01);//soft reset
	vTaskDelay(1/portTICK_PERIOD_MS);
	mag_write_byte(0x4C,0x38);//Normal Mode,ODR=30Hz
	vTaskDelay(1/portTICK_PERIOD_MS);
	mag_write_byte(0x4E,0x84);//X, Y, Z-Axis enabled
	vTaskDelay(1/portTICK_PERIOD_MS);
	mag_write_byte(0x51,0x04);// No. of Repetitions for X-Y Axis = 9
	vTaskDelay(1/portTICK_PERIOD_MS);
	printf("finish init BMX055\n");
}
void BMX055::get_raw_data(vect*accl,vect*gyro,vect*mag){
	uint8_t accl_data[6],gyro_data[6];
    uint16_t m0[8];
    //read accl
    if(accl)for(int i=0;i<6;++i)accl_data[i]=accl_read_byte(0x02+i);
    //read gyro
    if(gyro)for(int i=0;i<6;++i)gyro_data[i]=gyro_read_byte(0x02+i);
    //read mag
    if(mag)for(int i=0;i<8;++i)m0[i]=mag_read_byte(0x42+i);
    //calculate accl
    if(accl){
        int a0[3];

        for(int i=0;i<3;++i){
        	a0[i]=(int)accl_data[i*2]+(int)(accl_data[2*i+1]<<4);
        	if(a0[i]>2047)a0[i]-=4096;
        	(*accl)[i]=((float)a0[i])/1024;//convert to -1~1
        	(*accl)[i]*=-1;
        }
    }
    //calculate gyro
    if(gyro){
        int om0[3];
        for(int i=0;i<3;++i){
            om0[i]=(int)gyro_data[i*2]+(int)(gyro_data[i*2+1]<<8);
            if(om0[i]>32767)om0[i]-=65536;
    		(*gyro)[i]=((float)om0[i])*0.00026632;//500*3.141592/(180*32767)=0.00026632;
    		//convert to -500deg/s~500deg/s
        }
    }
    //calculate mag
    if(mag){
        int mx=(m0[1]<<5)|(m0[0]>>3);
        int my=(m0[3]<<5)|(m0[2]>>3);
        int mz=(m0[5]<<7)|(m0[4]>>1);
        if(mx>4095)mx-=8192;
        if(my>4095)my-=8192;
        if(mz>16383)mz-=32768;
        (*mag)[1]=(float)mx;
        (*mag)[0]=-(float)my;
        (*mag)[2]=(float)mz;
    }
}
void BMX055::get_IMU_data(IMU_data_t*data){
	get_raw_data(&data->accl,&data->gyro,&data->mag);
	if(f_enable_accl_buf)data->accl=accl_buf.cal(data->accl);
	if(f_enable_gyro_buf)data->gyro=gyro_buf.cal(data->gyro);
	if(f_enable_mag_buf)data->mag=mag_buf.cal(data->mag);

	data->gyro-=gyro_offset;
	data->mag-=mag_offset;
}

void BMX055::set_gyro_offset(const vect&v){
	gyro_offset=v;
}
void BMX055::set_mag_offset(const vect&v){
	mag_offset=v;
}
vect BMX055::get_gyro_offset()const{
	return gyro_offset;
}
vect BMX055::get_mag_offset()const{
	return mag_offset;
}




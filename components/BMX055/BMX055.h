/*
 * BMX055.h
 *
 *  Created on: 22 Jul 2020
 *      Author: yuta2
 */

#ifndef BMX055_BMX055_H_
#define BMX055_BMX055_H_

#include"VMA.h"
#include"vect.h"
extern "C"{
#include"freertos/FreeRTOS.h"
#include"freertos/task.h"
#include"freertos/queue.h"
}

struct IMU_config_t{
	uint8_t accl_ma_size;
	uint8_t gyro_ma_size;
	uint8_t mag_ma_size;
};
struct IMU_data_t{
	vect accl,gyro,mag;
};
class BMX055{
	vect gyro_offset,mag_offset;
	VMA accl_buf,gyro_buf,mag_buf;
	bool f_enable_accl_buf,f_enable_gyro_buf,f_enable_mag_buf;
public:
	BMX055();
	void init(IMU_config_t* = NULL);
	void set_gyro_offset(const vect&v);
	void set_mag_offset(const vect&v);
	vect get_gyro_offset()const;
	vect get_mag_offset()const;
	void get_raw_data(vect*paccl,vect*pgyro,vect*pmag);
	void get_IMU_data(IMU_data_t*);
};

extern BMX055 imu;

#endif /* BMX055_BMX055_H_ */

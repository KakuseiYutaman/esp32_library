/*
 * LCD.h
 *
 *  Created on: 7 Aug 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_LCD_LCD_H_
#define COMPONENTS_LCD_LCD_H_

#include"I2C.h"

class LCD{
	static I2C port;
	uint8_t addr;
	void send_instruct(uint8_t instruct);
	void send_data(uint8_t data);
	static void display(void *p);
	TaskHandle_t h_display;
	char str1[8],str2[8];
	uint8_t str1_len,str2_len;
public:
	void init(gpio_num_t sda_pin,gpio_num_t scl_pin,uint8_t addr);
	void print(const char*,const char*);
};


#endif /* COMPONENTS_LCD_LCD_H_ */

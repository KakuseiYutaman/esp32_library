/*
 * LCD.cpp
 *
 *  Created on: 7 Aug 2020
 *      Author: yuta2
 */
#include"LCD.h"
#include<string.h>

I2C LCD::port;
bool f_i2c_init=0;

void LCD::send_instruct(uint8_t instruct){
	uint8_t msg[2]={0x00,instruct};
	port.master_write(msg,2,addr);
}
void LCD::send_data(uint8_t data){
	uint8_t msg[2]={0x40,data};
	port.master_write(msg,2,addr);
}
void LCD::init(gpio_num_t sda_pin,gpio_num_t scl_pin,uint8_t addr){
	this->addr=addr;
	if(!f_i2c_init){
		port.init(sda_pin,scl_pin,400000);
		f_i2c_init=1;
	}
	vTaskDelay(40/portTICK_PERIOD_MS);//wait for lcd Vdd get stable
	send_instruct(0x38);
	vTaskDelay(1/portTICK_PERIOD_MS);
	send_instruct(0x39);
	vTaskDelay(1/portTICK_PERIOD_MS);
	send_instruct(0x14);
	vTaskDelay(1/portTICK_PERIOD_MS);
	send_instruct(0x70);
	vTaskDelay(1/portTICK_PERIOD_MS);
	send_instruct(0x56);
	vTaskDelay(1/portTICK_PERIOD_MS);
	send_instruct(0x6c);
	vTaskDelay(200/portTICK_PERIOD_MS);
	send_instruct(0x38);
	vTaskDelay(1/portTICK_PERIOD_MS);
	send_instruct(0x0c);
	vTaskDelay(1/portTICK_PERIOD_MS);
	send_instruct(0x01);
	vTaskDelay(200/portTICK_PERIOD_MS);
	str1_len=str2_len=0;
	xTaskCreatePinnedToCore(display,"display",2048,this,10,&h_display,1);
	printf("finish LCD setup\n");
}
void LCD::print(const char* str1,const char* str2){
	memcpy(this->str1,str1,8);
	memcpy(this->str2,str2,8);
	xTaskNotify(h_display,0,eNoAction);
}
void LCD::display(void*p){
	LCD* plcd=(LCD*)p;
	while(1){
		BaseType_t rs=xTaskNotifyWait(0,0,NULL,portMAX_DELAY);
		if(rs){
			plcd->send_instruct(0x01);//set line
			vTaskDelay(10/portTICK_PERIOD_MS);
			for(int i=0;i<8&&plcd->str1[i];++i){
				plcd->send_data(plcd->str1[i]);
			}
			plcd->send_instruct(0x80+0x40);
			vTaskDelay(10/portTICK_PERIOD_MS);
			for(int i=0;i<8&&plcd->str2[i];++i){
				plcd->send_data(plcd->str2[i]);
			}
		}
		vTaskDelay(1);
	}
}

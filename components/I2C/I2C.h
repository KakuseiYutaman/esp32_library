/*
 * MyI2C.h
 *
 *  Created on: 14 May 2020
 *      Author: yuta2
 */

#ifndef COMPONENTS_POSTURE_BMX055_MYI2C_MYI2C_H_
#define COMPONENTS_POSTURE_BMX055_MYI2C_MYI2C_H_
extern "C"{
#include"driver/i2c.h"
#include"driver/gpio.h"
}

class I2C{
	i2c_port_t port;
public:
	void init(gpio_num_t sda_pin,gpio_num_t scl_pin,uint32_t clk_speed);
	void master_write(uint8_t*data,size_t size,uint8_t slave_addr);
	void master_read(uint8_t*data,size_t size,uint8_t slave_addr);
};



#endif /* COMPONENTS_POSTURE_BMX055_MYI2C_MYI2C_H_ */

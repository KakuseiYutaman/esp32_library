/*
 * MyI2C.c
 *
 *  Created on: 30 Apr 2020
 *      Author: yuta2
 */

#include "I2C.h"

#define DEFAULT_RX_BUFFER_SIZE 10
#define DEFAULT_TX_BUFFER_SIZE 10

void I2C::init(gpio_num_t sda_pin,gpio_num_t scl_pin,uint32_t clk_speed){
	i2c_config_t conf;
	conf.master.clk_speed=clk_speed;
	conf.mode=I2C_MODE_MASTER;
	conf.scl_io_num=scl_pin;
	conf.sda_io_num=sda_pin;
	conf.scl_pullup_en=GPIO_PULLUP_ENABLE;
	conf.sda_pullup_en=GPIO_PULLUP_ENABLE;
	i2c_param_config(port,&conf);
	printf("i2c driver install\n");
	i2c_driver_install(port,I2C_MODE_MASTER,0,0,0);//portで指定されたi2cポートをマスターモードでインストール。バッファはRX,TXともに使わないので0、割り込みのなんかが０
}
void I2C::master_write(uint8_t*data,size_t size,uint8_t slave_addr){
	i2c_cmd_handle_t cmd=i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd,(slave_addr<<1)|I2C_MASTER_WRITE,1);//last arg mean ACK enable
	i2c_master_write(cmd,data,size,1);////last arg mean ACK enable
	i2c_master_stop(cmd);
	i2c_master_cmd_begin(port,cmd,1000/portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
}
void I2C::master_read(uint8_t*data,size_t size,uint8_t slave_addr){
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (slave_addr << 1) | I2C_MASTER_READ, 1);//ACK enable
	if (size > 1) {
		i2c_master_read(cmd, data, size - 1, I2C_MASTER_ACK);
	}
	i2c_master_read_byte(cmd, data + size - 1, I2C_MASTER_NACK);
	i2c_master_stop(cmd);
	i2c_master_cmd_begin(port, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
}


